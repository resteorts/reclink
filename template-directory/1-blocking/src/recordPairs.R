# Authors:	RCS, PB
# Maintainers: RCS
# Copyright: 2016-09-09, RCS, GPL v2 or later
# Version 1.0
# =====================================
#recordPairs.R creates a matrix (or data.frame) of pairs of records (all-to-all comparison) of records for blocking purposes.
# To run this file from the command line, run
# make -f src/Makefile
# For more information on Makefiles, please see
# http://robjhyndman.com/hyndsight/makefiles/


#TODO: Add extensions onto this that perform more advanced blocking. 

# Perform comparison of two records
# Input: dataset
# Output is index1 and index2 in the form of a matrix
# or data.frame
library(proto)
library(argparse)


parser <- ArgumentParser()
parser$add_argument("--input")
parser$add_argument("--output")
args <- parser$parse_args()
print(args)

test.data <- read.csv(args$input, sep="|",header=TRUE)
print(nrow(test.data))

# Initiliaze the entities
# empty matrix n choose 2
# let z be the position in matrix, start at 1
# loop on i:1:nrow(test.data)
		# loop j:i+1 to nrow(test.data)
		# pairs[z, 1] <- test.data[i, 'id']
		# pairs[z, 2] <- test.data[j, 'id']
		# increment z
# let z be the position in the matrix
n <- nrow(test.data)
z <- 1
pairs <- matrix(data = NA, nrow = choose(n,2), ncol=2)
for (i in 1:(nrow(test.data) - 1))	{
	for (j in (i+1):nrow(test.data)){
		pairs[z, 1] <- test.data[i, "record.id"]
		pairs[z, 2] <- test.data[j, "record.id"]
		z <- z + 1	
	}
}

match <- 
test.data$cluster.id[pairs[,1]] ==
test.data$cluster.id[pairs[,2]]
match <- as.numeric(match)
print(table(match))

pairs <- as.data.frame(pairs, col.names=c("index1","index2", "match"))		
#  ATTN: Write pairs to output at .csv file
# Eventually make sure I zip these. 
#gzfile.handle <- gzfile(args$ouput, open="wb")
#write.table(pairs, file=gzfile.handle, sep="|", 
	#quote=FALSE, row.names=FALSE)   

pairs <- cbind(pairs, match)	  
	
write.table(pairs, file="pairs-all-comparisons.csv", sep="|", 
	quote=FALSE, row.names=FALSE)  	 



